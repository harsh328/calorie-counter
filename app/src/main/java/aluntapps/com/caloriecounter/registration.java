package aluntapps.com.caloriecounter;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class registration extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private static final String str[] = {"Name", "Activity", "Goal", "Goal Weight"};

    int flag=0;
    SQLiteDatabase db;

    private static String name,gender,activity,goal,goalweightstr,dob;
    private static Double height,weight,goalweight;
    regName objReg;
    regActivity objActivity;
    regGoal objGoal;
    regGoalWeight objGoalWeight;
    //regHeight objHeight;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        objReg=regName.newInstance();
        objActivity=regActivity.newInstance();
        objGoal=regGoal.newInstance();
        objGoalWeight=regGoalWeight.newInstance();
        //objHeight=regHeight.newInstance();

        db = openOrCreateDatabase("calorie.db", SQLiteDatabase.CREATE_IF_NECESSARY, null);
        try {
            db.execSQL("create table user(name varchar(30) Primary Key,height real(4) Not Null,weight real(5) Not Null,activity_level varchar(8) Not Null,goal varhcar(3) Not Null,budget int(4) Not Null,frequency int(1) Not Null,gender varchar(1) Not Null,bmi real(3) Not Null,dob date Not Null)");
            db.execSQL("create table category(cid integer Primary Key Autoincrement,cname varchar(15) Not Null)");
            db.execSQL("create table food_item(fid integer Primary Key Autoincrement,sname varchar(15) Not Null,fats real(3) not null,carbohydrate real(5) not null,protein real(5) not null,calorie int(5) not null,saturated_fat real(5) not null,polyunsaturated_fat real(5) not null,monounsaturated_fat real(5) not null,trans_fat real(5) not null,cholestrol real(5) not null,sodium real(5) not null,potassium real(5) not null,fibers real(5) not null,sugar real(5) not null,vitamin_A real(5) not null,vitamin_C real(5) not null,calcium real(5) not null,iron real(5) not null,cid integer)");//foreign key enable

            //use database class create that class at the time of splashscreen

        } catch (Exception ex) {
            Toast.makeText(registration.this,"Not created",Toast.LENGTH_LONG).show();
        }


        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Name");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }

        });

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                toolbar.setTitle(str[position]);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        /*TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.regmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.next_button:
                if (mViewPager.getCurrentItem() != mViewPager.getChildCount()) {
                    mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
                    System.out.println(mViewPager.getChildCount()+mViewPager.getCurrentItem());
                    flag++;
                    if(flag==4){
                        /*Toast.makeText(registration.this,name+" "+height+" "+weight+" "+gender,Toast.LENGTH_LONG).show();
                        Toast.makeText(registration.this,activity,Toast.LENGTH_LONG).show();
                        Toast.makeText(registration.this,goal,Toast.LENGTH_LONG).show();
                        Toast.makeText(registration.this,goalweight+" "+goalweightstr,Toast.LENGTH_LONG).show();*/
                        ContentValues cv=new ContentValues();
                        cv.put("name",name);
                        cv.put("height",55);
                        cv.put("weight",25);
                        cv.put("activity_level","lightly");
                        cv.put("goal",75);
                        cv.put("budget",2000);
                        cv.put("frequency",3);
                        cv.put("gender","M");
                        cv.put("bmi",20);
                        cv.put("dob",dob);
                        db.insert("user",null,cv);
                        db.close();
                        Intent i=new Intent(registration.this,Home.class);
                        startActivity(i);
                    }
                    /*else {
                        Toast.makeText(registration.this,"Else",Toast.LENGTH_LONG).show();
                        Intent i=new Intent(registration.this,Home.class);
                        startActivity(i);
                    }*/
                }
                break;
            case android.R.id.home:
                if (mViewPager.getCurrentItem() != 0) {
                    mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
                }
                else
                    super.onBackPressed();
        }
        return true;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if(id==999){
            return new DatePickerDialog(this,objReg.myDateListener,objReg.year,objReg.month,objReg.day);
        }
        return super.onCreateDialog(id);
    }

    public static class regName extends Fragment {
        EditText edName,edHeight,edWeight;
        RadioGroup rgrpGender;
        int day,month,year;
        Button btnDOB;
        public static regName newInstance() {

            //Bundle args = new Bundle();

            regName fragment = new regName();
            //fragment.setArguments(args);
            return fragment;
        }

        private DatePickerDialog.OnDateSetListener myDateListener= new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                month=m;
                day=d;
                year=y;
                btnDOB.setText(year+"-"+(month+1)+"-"+day);
                dob=btnDOB.getText().toString();
            }
        };
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.name, container, false);
            btnDOB= (Button)view.findViewById(R.id.btnDOB);
            Calendar calendar = GregorianCalendar.getInstance();

            day = calendar.get(Calendar.DAY_OF_MONTH);
            month=calendar.get(Calendar.MONTH);
            year=calendar.get(Calendar.YEAR);

            btnDOB.setText(day+"/"+(month+1)+"/"+year);
            btnDOB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().showDialog(999);
                }
            });
            edName=(EditText)view.findViewById(R.id.edName);
            edHeight=(EditText)view.findViewById(R.id.edHeight);
            edWeight=(EditText)view.findViewById(R.id.edWeight);
            rgrpGender=(RadioGroup)view.findViewById(R.id.rgrpGender);

            edName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    name=edName.getText().toString();
                }
            });

            edHeight.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    height=Double.parseDouble(edHeight.getText().toString());
                }
            });

            edWeight.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    weight=Double.parseDouble(edWeight.getText().toString());
                }
            });

            rgrpGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId){
                        case R.id.rbtnMale:
                            gender="Male";
                            break;
                        case R.id.rbtnFemale:
                            gender="Female";
                            break;
                    }
                }
            });
            return view;
        }
    }

    /*public static class regHeight extends Fragment{

        EditText edHeight,edWeight;

        public static regHeight newInstance() {

            //Bundle args = new Bundle();

            regHeight fragment = new regHeight();
            //fragment.setArguments(args);
            return fragment;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view=inflater.inflate(R.layout.height,container,false);
            edHeight=(EditText)view.findViewById(R.id.edHeight);
            edWeight=(EditText)view.findViewById(R.id.edWeight);
            edHeight.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    height=Double.parseDouble(edHeight.getText().toString());
                }
            });

            edWeight.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    weight=Double.parseDouble(edWeight.getText().toString());
                }
            });
            return view;
        }
    }*/

    public static class regActivity extends Fragment {

        public static regActivity newInstance() {

            //Bundle args = new Bundle();

            regActivity fragment = new regActivity();
            //fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.activity, container, false);
            RadioGroup rgrpAct=(RadioGroup)view.findViewById(R.id.rgrpActivity);
            rgrpAct.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch(checkedId){
                        case R.id.rbtnNA:
                            activity="Not active";
                            break;
                        case R.id.rbtnLA:
                            activity="Lightly active";
                            break;
                        case R.id.rbtnActive:
                            activity="Active";
                            break;
                        case R.id.rbtnVA:
                            activity="Very Active";
                            break;
                    }
                }
            });
            return view;
        }
    }

    public static class regGoal extends Fragment {

        public static regGoal newInstance() {

            //Bundle args = new Bundle();

            regGoal fragment = new regGoal();
            //fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.goal, container, false);
            RadioGroup rgrpgoal=(RadioGroup)view.findViewById(R.id.rgrpGoal);
            rgrpgoal.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch(checkedId){
                        case R.id.rbtnLose:
                            goal="Lose weight";
                            break;
                        case R.id.rbtnGain:
                            goal="Gain weight";
                            break;
                        case R.id.rbtnMaintain:
                            goal="Maintain weight";
                            break;
                    }
                }
            });
            return view;
        }
    }

    public static class regGoalWeight extends Fragment {

        EditText edweight;
        public static regGoalWeight newInstance() {

            //Bundle args = new Bundle();

            regGoalWeight fragment = new regGoalWeight();
            //fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.goalweight, container, false);
            RadioGroup rgrpweight=(RadioGroup)view.findViewById(R.id.rgrpGoalWeight);
            edweight=(EditText)view.findViewById(R.id.edGoalWeight);
            edweight.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    goalweight=Double.parseDouble(edweight.getText().toString());
                }
            });
            rgrpweight.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch(checkedId){
                        case R.id.radioButton:
                            goalweightstr="Gain 0.25";
                            break;
                        case R.id.radioButton2:
                            goalweightstr="Gain 0.50";
                            break;
                        case R.id.radioButton3:
                            goalweightstr="Gain 0.75";
                            break;
                        case R.id.radioButton4:
                            goalweightstr="Gain 1";
                            break;
                    }
                }
            });
            return view;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return objReg;
                //case 1:
                    //return objHeight;
                case 1:
                    return objActivity;
                case 2:
                    return objGoal;
                case 3:
                    return objGoalWeight;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 5 total pages.
            return 4;
        }

        /*@Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
                case 3:
                    return "SECTION 4";
            }
            return null;
        }*/
    }
}
