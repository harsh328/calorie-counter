package aluntapps.com.caloriecounter;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class SplashScreen extends AppCompatActivity {

    Button reg;
    int splashTime=3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        reg=(Button)findViewById(R.id.btnReg);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Intent i=new Intent(SplashScreen.this,Home.class);
                //startActivity(i);
                //SplashScreen.this.finish();
                reg.setVisibility(View.VISIBLE);
            }
        },splashTime);

        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SplashScreen.this,registration.class);
                startActivity(i);
                SplashScreen.this.finish();
            }
        });
    }
}
